#-------------------------------------------------
#
# Project created by QtCreator 2015-11-02T15:34:02
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SPTool
TEMPLATE = app


SOURCES += main.cpp\
        sptool.cpp

HEADERS  += sptool.h

FORMS    += sptool.ui
