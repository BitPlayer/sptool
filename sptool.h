#ifndef SPTOOL_H
#define SPTOOL_H

#include <QMainWindow>

namespace Ui {
class SPTool;
}

class SPTool : public QMainWindow
{
    Q_OBJECT

public:
    explicit SPTool(QWidget *parent = 0);
    ~SPTool();

private slots:
    void on_btn_quit_clicked();

private:
    Ui::SPTool *ui;
};

#endif // SPTOOL_H
