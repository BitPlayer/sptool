#include "sptool.h"
#include "ui_sptool.h"

SPTool::SPTool(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SPTool)
{
    ui->setupUi(this);
}

SPTool::~SPTool()
{
    delete ui;
}

void SPTool::on_btn_quit_clicked()
{
    this->close();
}
